#!/bin/bash
#
#--------------------------------------------------------------------------
# Tries to attach, creates fs if necessary, and mount related volumes
#--------------------------------------------------------------------------
#
# Based on instance and volumes tags
# - attach volumes related to the instance
# - mount volumes on pre-defined mounting points
#
# @author: Rodrigo Vieira - rodrigodelimavieira@gmail.com
#------------------------------------------------------------------------------

TASK_NAME="automount"

instance_id=$( getId )
instance_region=$( getRegion )
instance_az=$( getAz )
private_hostname=$( getPrivateHostname $instance_region )
# Look for all detached EC2 volumes where
# volume's "Name" tag is equal to this instance's "Name" tag
logdebug "Looking for available volumes"
volume_ids=$( getDetachedVolumesByName $instance_az $private_hostname )

# For each found volume check if it's in the same instance's AZ
for volume_id in $volume_ids;do
    volume_az=$(aws ec2 describe-volumes \
        --volume-ids $volume_id \
        --query Volumes[0].AvailabilityZone \
        --output text)

    loginfo "Found an available volume: $volume_id"

    if [ "$volume_az" != "$instance_az" ];then
        logwarn "The volume $volume_id can't be attached. Instance and Volume must be in the same Availability Zone"
    else
        # Attach volume
        logdebug "Attaching volume $volume_id"
        device_name=$( getNextAvailableDeviceName $instance_id )
        aws ec2 attach-volume --device $device_name --instance-id $instance_id --volume-id $volume_id

        # wait 15s for attaching process gets finished
        sleep 15

        mount_point=$( getTagValueFromVolume "MountPoint" $volume_id )
        file_system=$( getTagValueFromVolume "FileSystem" $volume_id )
        [ -z "$file_system" ] && file_system="ext4"

        logdebug "Creating mounting point: $mount_point"
        mkdir -p $mount_point

        # Is the mount_point already used?
        if [ ! -z "$(ls -A $mount_point)" ];then
            logwarn "Can't use $mount_point to mount $volume_id. Directory is not empty."
        else

            logdebug "Picking a device name for $volume_id"
            instance_device_name=$( getInstanceDeviceName $volume_id )

            # If can't get device name, give up and try the next volume
            if [ -z "$instance_device_name" ];then
                logerror "Attached volume: $volume_id not found as a Linux device"
                continue
            fi

            # blkid returns non-zero if device has not any filesystem
            blkid $instance_device_name || mkfs -t $file_system $instance_device_name
            uuid=$(blkid $instance_device_name | cut -d'"' -f2)
            # If can't get UUID, give up and try the next volume
            if [ -z "$uuid" ];then
                logerror "Could not get the UUID for $instance_device_name"
                continue
            fi

            # Persiste this mouting point on fstab
            echo "UUID=$uuid $mount_point            $file_system    defaults    0 0" >> /etc/fstab

            # Mount volume
            logdebug "Mounting $volume_id on $mount_point"
            mount $mount_point
            loginfo "$volume_id mounted at $mount_point"
        fi
    fi
done
