#!/bin/bash
#
#--------------------------------------------------------------------------
# Creates Route53 record according to "Name" tag
#--------------------------------------------------------------------------
#
# Based on instance meta-data:
# - set an A resource record on Route53 internal zone
#
# @author: Rodrigo Vieira - rodrigodelimavieira@gmail.com
#------------------------------------------------------------------------------

TASK_NAME="autoname"

# Requirements to proceed with this script
region=$( getRegion )
instanceTagName=$( getPrivateHostname $region )
currentHostname=$(hostnamectl status --static)
privateZoneName=$( getPrivateZoneName $region )
privateIpAddress=$( getPrivateIpAddress $region )
resolvedIp=$( getIpByName $instanceTagName )

# If hostname has changed
if [ "$currentHostname" != "$instanceTagName" ] || [ "$privateIpAddress" != "" ];then
  # Change instance hostname
  loginfo "Updating hostname to $instanceTagName"
  hostnamectl set-hostname $instanceTagName

  # Update or insert Route53 record for this instance hostname
  route53Upsert $privateZoneName $instanceTagName $privateIpAddress

fi
