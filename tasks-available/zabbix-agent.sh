#!/bin/bash
#
#--------------------------------------------------------------------------
# Updates hostname in zabbix agent settings
#--------------------------------------------------------------------------
#
# @author: Rodrigo Vieira - rodrigodelimavieira@gmail.com
#------------------------------------------------------------------------------

TASK_NAME="zabbix-agent"

region=$( getRegion )
privateHostname=$( getPrivateHostname $region )

# Updates hostname into zabbix-agent configuration file if exists
if [ -f "/etc/zabbix/zabbix_agentd.conf" ];then
  loginfo "Set hostname on O.S and zabbix files"
  sed -i "s/Hostname=.*$/Hostname=$privateHostname/" /etc/zabbix/zabbix_agentd.conf
fi
