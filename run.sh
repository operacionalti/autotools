#!/bin/bash
#
#--------------------------------------------------------------------------
# Run enabled tasks
#--------------------------------------------------------------------------
#
# Based on task-enabled/ symlinks or .env's task list
# run enabled tasks.
#
# @author: Rodrigo Vieira - rodrigodelimavieira@gmail.com
#------------------------------------------------------------------------------

BASEDIR=$(dirname $0)
source "$BASEDIR/lib/bootstrap.sh"
cd $BASEDIR
for task in $(ls $BASEDIR/tasks-enabled);do
    source $BASEDIR/tasks-enabled/$task
done
