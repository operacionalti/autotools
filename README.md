# How to use it

1. Clone this repo on `/root/autotools`.
2. Make your own `/root/.env` based on `.env-default`.
3. Add the `run.sh` script on your `/etc/crontab` like so:

```
@reboot     root    bash    /root/autotools/run.sh
```

4. Protect your data:

```bash
chown -R root.root /root/autotools /root/.env
chmod 0640 /root/.env
find /root/autotools -type d -exec chmod 0750 {} \;
find /root/autotools -type f -exec chmod 0640 {} \;
```

The logs of autotools tasks will be available at `/var/log/scripts`.
