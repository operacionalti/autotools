#!/bin/bash
#
#--------------------------------------------------------------------------
# Common AWS routines
#--------------------------------------------------------------------------
#
# Extract information from instance meta-data or runnin awscli commands.
#
# @author: Rodrigo Vieira - rodrigodelimavieira@gmail.com
#------------------------------------------------------------------------------

# Try to find where is the aws binary
if [ -f "/usr/loca/bin/aws" ];then
  aws="/usr/local/bin/aws"
elif [ -f "/usr/bin/aws" ];then
  aws="/usr/bin/aws"
else
  aws="aws"
fi

# Return the [id] of an instance from its meta-data.
function getId {
    local _id=$(curl --silent http://169.254.169.254/latest/meta-data/instance-id)

    echo $_id
}

# Return instance's [region] from its meta-data.
function getRegion {
    local _region=$(curl --silent http://169.254.169.254/latest/dynamic/instance-identity/document | jq -r .region)

    echo $_region
}

# Return the instance [availability zone] from its meta-data.
function getAz {
    local _az=$(curl --silent http://169.254.169.254/latest/dynamic/instance-identity/document |jq -r .availabilityZone)

    echo $_az
}


# Returns the value of the [Name tag] of an instance
function getPrivateHostname {
    local _region="$1"
    local _privateHostname=$($aws ec2 describe-tags \
    --filters \
        "Name=resource-id,Values=$(curl -s http://169.254.169.254/latest/meta-data/instance-id)" \
        "Name=key,Values=Name" \
    --query 'Tags[0].Value' \
    --region $_region \
    --output text)

    echo $_privateHostname
}


# Returns the value of the [Domain tag] of an instance
function getPrivateZoneName {
    local _region="$1"
    local _privateZoneName=$($aws ec2 describe-tags \
    --filters \
        "Name=resource-id,Values=$(curl -s http://169.254.169.254/latest/meta-data/instance-id)" \
        "Name=key,Values=Domain" \
    --query 'Tags[0].Value' \
    --region $_region \
    --output text)

    # Default domain
    if [[ "$_privateZoneName" =~ ^(null|None)$ ]];then
      _privateZoneName="$(getenv DOMAIN)"
    fi

    echo $_privateZoneName
}

# Returns the [private ip] address of an instance from its meta-data.
function getPrivateIpAddress {
    local _region="$1"
    local _privateIpAddress=$($aws ec2 describe-instances \
    --filters \
        "Name=instance-id,Values=$(curl -s http://169.254.169.254/latest/meta-data/instance-id)" \
    --query 'Reservations[0].Instances[0].PrivateIpAddress' \
    --region $_region \
    --output text)

    echo $_privateIpAddress
}

# Returns the [Route53 Zone ID] of a private zone name.
function getHostedZoneId {
    local _region="$1"
    local _privateZoneName="$2"
    local _hostedZoneId=$($aws route53 list-hosted-zones-by-name \
    --dns-name $_privateZoneName \
    --max-items 1 \
    --query "HostedZones[0].Id" \
    --region $_region \
    --output text)

    echo $_hostedZoneId
}

# Return [volume ids] that are in detached state.
function getDetachedVolumesByName {
    local _az="$1"
    local _name="$2"

    local _volumes=$($aws ec2 describe-volumes \
    --filters \
        Name=tag-key,Values="Name" Name=tag-value,Values="$_name*" \
        Name=status,Values="available" \
        Name=availability-zone,Values="$_az" \
        --query "Volumes[*].VolumeId" \
        --output text)

    echo "$_volumes"
}

# Return the [value of a tag] of a volume.
function getTagValueFromVolume {
    local _tag_name="$1"
    local _volume_id="$2"

    local _tag_value=$(aws ec2 describe-volumes \
        --volume-ids $_volume_id \
        --query "Volumes[0].Tags[?Key=='$_tag_name'].Value|[0]" \
        --output text)

    echo "$_tag_value"
}

# Return the [value of a tag] of an instance.
function getTagValueFromInstance {
    local _tag_name="$1"
    local _instance_id="$2"

    if [ -z "$_instance_id" ];then
        _instance_id=$( getId )
    fi

    local _tag_value=$(aws ec2 describe-instances \
        --instance-ids $_instance_id \
        --query "Reservations[0].Instances[0].Tags[?Key=='$_tag_name'].Value|[0]" \
        --output text)

    echo "$_tag_value"
}

# Return the [propably next available device name] to be used
# when attaching a volume to an instance.
function getNextAvailableDeviceName {
    local _instance_id="$1"
    # get next available device name
    used_devices=$(aws ec2 describe-instance-attribute \
        --attribute blockDeviceMapping \
        --instance-id $_instance_id \
        --query "BlockDeviceMappings[*].DeviceName" \
        --output text)
    device_names=( sdf sdg sdh sdi sdj sdk skl sdm sdn sdo sdp )
    for device_name in "${device_names[@]}";do
        echo $used_devices | grep -q $device_name || break
    done

    echo "/dev/$device_name"
}

# Return the [size] of a volume.
function getVolumeSize {
    local _volume_id="$1"
    _size=$(aws ec2 describe-volumes \
        --volume-id $_volume_id \
        --query "Volumes[0].Size" \
        --output text)

    echo "$_size"
}

# Try to get the [volume name] by its size.
function getInstanceDeviceNameBySize {
    _volume_id="$1"
    _size="$( getVolumeSize $_volume_id )"
    _size_in_bytes=$(( $_size * 1024 * 1024 * 1024 ))
    _available_devices=$(fdisk -l |grep -v loop |grep '/dev/' |grep sectors | egrep "( $_size | $_size_in_bytes )" |sed -r 's/.*\/dev\/([^: ]+).*/\1/g')

    for _possible_device in $_available_devices;do
        _mountpoint=$(lsblk /dev/$_possible_device --noheadings --raw -o MOUNTPOINT)
        # Is the device umounted?
        if [ -z "$_mountpoint" ];then
            _device="/dev/$_possible_device"
            break
        fi
    done

    echo "$_device"
}

# Returns the [device name]
function getInstanceDeviceName {
    local _volume_id="$1"
    local _device=$( getInstanceDeviceNameBySize $_volume_id )

    echo "$_device"
}

# Creates an entry like "server01.internal.acme.net"
# pointing to the private IP address of this instances
function route53Upsert {
  if [ "$#" -lt 3 ];then
    logerror "Less than three arguments passed to route53Upsert function"
    exit 1
  fi
  local _zonename="$1"
  local _hostname="$2"
  local _ipaddress="$3"

  local _zoneid=$( getHostedZoneId $region $_zonename )

  loginfo "Updating/Inserting '$_hostname.$_zonename IN A $_ipaddress' resource record."

  $aws route53 change-resource-record-sets \
      --hosted-zone-id "$_zoneid" \
      --change-batch '{"Comment": "A new record set for the zone.","Changes": [{"Action": "UPSERT","ResourceRecordSet": {"Name": "'$_hostname.$_zonename'","Type": "A","TTL": 60,"ResourceRecords": [{"Value": "'$_ipaddress'"}]}}]}' \
      --region $region

    echo "$?"
}
