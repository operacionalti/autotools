#!/bin/bash
#
#--------------------------------------------------------------------------
# Standard bootstraping for autotools scripts
#--------------------------------------------------------------------------
#
#
# @author: Rodrigo Vieira - rodrigodelimavieira@gmail.com
#------------------------------------------------------------------------------

source "$BASEDIR/lib/utils.sh"
source "$BASEDIR/lib/aws.sh"

[ -f "$BASEDIR/.env" ] && source "$BASEDIR/.env"

mkdir -p $LOG_DIR
