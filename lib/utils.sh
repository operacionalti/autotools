#!/bin/bash

#--------------------------------------------------------------------------
# Common functions for general scripts
#
# @author: Rodrigo Vieira - rodrigodelimavieira@gmail.com
#
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# Global settings
#------------------------------------------------------------------------------

LOG_DIR="/var/log/scripts"

#------------------------------------------------------------------------------
# Standard log messages
#------------------------------------------------------------------------------
#
# Write messages to standard output and also to a log file.
# Log messages will have the following pattern:
#
# date_and_time hostname scriptname: log_level - log_message
#
# for instance:
# Feb 20 13:34:01 web0 dbbackup: INFO - backup script finished without errors
#------------------------------------------------------------------------------

log () {
    _level="$1"
    _msg="$2"
    _datetime=$(date +"%b %e %T")
    _script=$(basename $0 | cut -d'.' -f1)
    if [ -n "$TASK_NAME" ];then
        _script="$TASK_NAME"
    fi
    _prefix="$_datetime $(hostname -s) $_script: $_level -"
    _file="$LOG_DIR/${_script}.log"

    echo "$_prefix $_msg" |tee -a $_file
}

#------------------------------------------------------------------------------
# Log levels
#------------------------------------------------------------------------------

# if DEBUG is set to true within client script.
logdebug () {
    if ( $DEBUG );then
        log DEBUG "$@"
    fi
}

loginfo () {
    log INFO "$@"
}

logwarning () {
    log WARNING "$@"
}

logerror () {
    log ERROR "$@"
}

logwarn () {
    logwarning "$@"
}

logerr () {
    logerror "$@"
}

#------------------------------------------------------------------------------
# Get Environment Variables
#------------------------------------------------------------------------------

getenv () {
    # look for a variable on shell environment, within a .env file,
    # or use the informed default value
    _env_var="$1"
    _default_value="$2" # or ""
    _dot_env="/root/.env"

    # is it defined on current environment?
    if [ -n "${!_env_var}" ];then
        echo "${!_env_var}"
    else
        # otherwise, try to load .env file
        [ -f "$_dot_env" ] && source  $_dot_env

        if [ -n "${!_env_var}" ];then
            echo "${!_env_var}"
        else
            # or just return the default value
            echo "$_default_value"
        fi
    fi
}

# Get current primary IP address
function getIpByName {

    local _hostname="$1"

    _ip=$(getent ahostsv4 $_hostname|head -1|cut -d' ' -f1)

    echo "$_ip"
}


#------------------------------------------------------------------------------
# Make a pid/lock file
#------------------------------------------------------------------------------

make_pid () {
    _pid_file="/run/$(basename $0 |cut -d'.' -f1).pid"

    if [ -f $_pid_file ]; then
        _pid="$(cat $_pid_file)"
        ps -p $_pid &> /dev/null
        if [ $? -eq 0 ]; then
            logerror "$_pid_file exists and the process with $_pid pid is running"
            exit
        fi
    fi

    # When pid is not a running process or _pid_file doesn't exist
    _pid=$$
    echo $_pid > $_pid_file

    echo  $_pid_file
}
